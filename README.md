# Description #

This is a script to download the resources for [https://gitlab.com/ntfwc/godot-megaman-bn-overworld](https://gitlab.com/ntfwc/godot-megaman-bn-overworld). It must be run within the directory of that project to work.

If you are on Windows, you can copy download_resources.py and download_resources.bat to the project directory and double-click download_resources.bat.

# Dependencies #
* Python3
* Python Imaging Library (Optional)
	* Without it, you have to convert some images manually
