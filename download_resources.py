#!/usr/bin/python3
import urllib.request
import http.client
import hashlib
import os.path
from io import BytesIO

CONTENT_LENGTH_HEADER = "content-length"
MAX_CONTENT_LENGTH = 100000

def download_file(url):
    with urllib.request.urlopen(url) as u:
        if u.code !=  http.client.OK:
            raise IOError("Response from URL '%s' returned exit code %d" % (url, u.code))

        info = u.info()
        if not CONTENT_LENGTH_HEADER in info:
            raise IOError("Response from URL '%s' has no content length" % url)
        
        content_length = int(info[CONTENT_LENGTH_HEADER])
        if content_length > MAX_CONTENT_LENGTH:
            raise IOError("Response from URL '%s' has a content length, %d, that exceeds the max" % (url, content_length))
        
        data = u.read(content_length)
        if len(data) != content_length:
            raise IOError("Failed to read the advertised content-length from URL '%s'" % url)

        return data

def calc_hash(data):
    digest = hashlib.sha256()
    digest.update(data)
    return digest.hexdigest()

class ImageDownloader(object):
    def __init__(self, filePath, url, fileHash):
        self.filePath = filePath
        self.url = url
        self.fileHash = fileHash

    def handle_file(self):
        if os.path.exists(self.filePath):
            print("Skipping '%s', it already exists" % self.filePath)
            return
        
        data = download_file(self.url)
        if calc_hash(data) != self.fileHash:
            print("Downloading '%s' failed, the hash was incorrect" % self.filePath)
            return
        
        self._write_file(data, self.filePath)

    def _write_file(self, data, filePath):
        print("Saving '%s'" % filePath)
        output_file=filePath + ".part"
        with open(output_file, "wb") as f:
            f.write(data)

        os.rename(output_file, filePath)


class ImageTranscodingDownloader(ImageDownloader):
    def __init__(self, filePath, fallbackFilePath, url, fileHash):
        super().__init__(filePath, url, fileHash)
        self.fallbackFilePath = fallbackFilePath

    def _write_file(self, data, filePath):
        if PIL == None:
            print("The file '%s' needs to be converted to '%s'" % (self.fallbackFilePath, self.filePath))
            super()._write_file(data, self.fallbackFilePath)
            return
        
        image = PIL.Image.open(BytesIO(data))
        print("Converting and saving '%s'" % self.filePath)
        image.save(self.filePath)

DOWNLOADERS=[
        ImageDownloader("megaman/spritesheet/megaman_spritesheet.png", "https://www.spriters-resource.com/resources/sheets/26/28875.png", "92f87d268f020dfab41aadc28503fb68e334b8cbcefa4bfb8abd199650d50d8f"),
        ImageDownloader("NPCs/Programs/spritesheet/mrProg.png", "http://www.sprites-inc.co.uk/files/EXE/EXE1/People/Misc/mrProg.png", "6171422c6890f816d33fe967904d4335b5bcf51b741655e208405813bb837c3e"),
        ImageTranscodingDownloader("NPCs/Programs/mugshot/MrProgmug_exe3.png", "NPCs/Programs/mugshot/MrProgmug_exe3.gif", "http://www.sprites-inc.co.uk/files/EXE/EXE3/Mugshots/MrProgmug_exe3.gif", "7e9a1f29404468c9b60629e328e63e753560df295473905f6cdd295771eb26dd"),
        ImageDownloader("maps/resources/LanHP/LanHP.png", "http://www.sprites-inc.co.uk/files/EXE/EXE3/Maps/Net/LanHP.png", "f5ad4e61b83673701f9cd3fea71a796f73aa49675a2e1130eac132cd9fbf6b18"),
        ImageDownloader("maps/resources/YokaSqr/YokaSqr.png", "http://www.sprites-inc.co.uk/files/EXE/EXE3/Maps/Net/YokaSqr.png", "332fd51a362daa121b4c79bb604ed68c9a38c3e301db0c415ff7115f79bad708"),
        ImageTranscodingDownloader("maps/resources/GenericHP/GenericHP.png", "maps/resources/GenericHP/GenericHP.gif", "http://www.sprites-inc.co.uk/files/EXE/EXE3/Maps/Net/GenericHP.gif", "d2f65fbce64bcfa2409c24e23852fc805fc2fc332253ee7c8f84d80541bfcd48")
            ]

PIL=None
try:
    import PIL.Image
except:
    print("Python Imaging Library not found, some images will need to be converted outside this script")

def main():
    if not os.path.exists("maps/resources") or not os.path.exists("megaman/spritesheet"):
        print("The working directory does not look correct. This must be run in the main directory of the godot-megaman-bn-overworld project")
        return

    for downloader in DOWNLOADERS:
        downloader.handle_file()

if __name__ == "__main__":
    main()
